## Setup instructions
1. Log on to Saturn and run 
``` sh
    module load gcc/10.2.0 cuda cmake
```
2. Clone this repository
``` sh
    git clone https://gitlab.com/cosc594/cuda-lab.git
```
3. Navigate to the repository, create a build directory and run cmake on the a04 node
``` sh
    cd cuda-lab
    mkdir build && cd build
    srun -wa04 cmake ..
```
4. Implement the axpy and dot kernels in `src/lab_examples.cu`. This is the only file you need to change.
5. When you want to build and run tests or benchmarks, please be sure to use the a04 queue by prefixing your commands with `srun -wa04`.
6. Build with
``` sh
    srun -wa04 make
```
7. Run tests with
``` sh
    srun -wa04 ctest -V tests/lab_examples_cuda
```
8. Run benchmarks with
``` sh
    srun -wa04 benchmark/lab_examples
```
