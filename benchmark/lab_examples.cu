#include <ginkgo/ginkgo.hpp>

#include <random>

#include "benchmark/benchmark.h"

#include "../include/lab_examples.hpp"
#include "test/utils.hpp"

namespace cuda {

template <typename ValueType>
static void bench_axpy(benchmark::State &st, [[maybe_unused]] ValueType deduction_help, kernel variant)
{
    using Vec = gko::matrix::Dense<ValueType>;
    auto exec = gko::ReferenceExecutor::create();

    size_type nelems = st.range(0);

    auto vec1 = gko::test::generate_random_matrix<Vec>(
        nelems, 1,
        std::normal_distribution<gko::remove_complex<ValueType>>(50, 5),
        std::normal_distribution<gko::remove_complex<ValueType>>(50, 5),
        std::ranlux48(42), exec);
    auto vec2 = gko::test::generate_random_matrix<Vec>(
        nelems, 1,
        std::normal_distribution<gko::remove_complex<ValueType>>(50, 5),
        std::normal_distribution<gko::remove_complex<ValueType>>(50, 5),
        std::ranlux48(42), exec);
    auto a = gko::test::generate_random_matrix<Vec>(
        1, 1,
        std::normal_distribution<gko::remove_complex<ValueType>>(50, 5),
        std::normal_distribution<gko::remove_complex<ValueType>>(50, 5),
        std::ranlux48(42), exec);
    ValueType *d_a, *d_x, *d_y;
    cudaMalloc( (void **) &d_a, sizeof(ValueType) );
    cudaMalloc( (void **) &d_x, nelems * sizeof(ValueType) );
    cudaMalloc( (void **) &d_y, nelems * sizeof(ValueType) );
    cudaMemcpy(d_a, a->get_const_values(), sizeof(ValueType), cudaMemcpyHostToDevice);
    cudaMemcpy(d_x, vec1->get_const_values(), nelems * sizeof(ValueType), cudaMemcpyHostToDevice);
    cudaMemcpy(d_y, vec2->get_const_values(), nelems * sizeof(ValueType), cudaMemcpyHostToDevice);

    for (auto _ : st) {
        if (variant == kernel::single_thread) {
            axpy_single_thread(d_a, d_x, nelems, d_y);
        } else if (variant == kernel::parallel_one || variant == kernel::parallel_two){
            axpy_parallel(d_a, d_x, nelems, d_y);
        }
        cudaDeviceSynchronize();
    }
    cudaFree(d_a);
    cudaFree(d_x);
    cudaFree(d_y);
}

template <typename ValueType>
static void bench_norm2(benchmark::State &st, [[maybe_unused]] ValueType deduction_help, kernel variant)
{
    using Vec = gko::matrix::Dense<ValueType>;
    auto exec = gko::ReferenceExecutor::create();

    size_type nelems = st.range(0);

    auto vec = gko::test::generate_random_matrix<Vec>(
        nelems, 1,
        std::normal_distribution<gko::remove_complex<ValueType>>(50, 5),
        std::normal_distribution<gko::remove_complex<ValueType>>(50, 5),
        std::ranlux48(42), exec);

    gko::remove_complex<ValueType> res = 0.0;
    ValueType *d_vec, *d_res;
    cudaMalloc( (void **) &d_res, sizeof(ValueType) );
    cudaMalloc( (void **) &d_vec, nelems * sizeof(ValueType) );
    cudaMemcpy(d_res, &res, sizeof(ValueType), cudaMemcpyHostToDevice);
    cudaMemcpy(d_vec, vec->get_const_values(), nelems * sizeof(ValueType), cudaMemcpyHostToDevice);
    for (auto _ : st) {
        if (variant == kernel::single_thread) {
            norm2_single_thread(d_vec, nelems, d_res);
        } else if (variant == kernel::parallel_one) {
            norm2_atomic(d_vec, nelems, d_res);
        } else if (variant == kernel::parallel_two){
            norm2_reduction(d_vec, nelems, d_res);
        }
        cudaDeviceSynchronize();
    }
    cudaFree(d_vec);
    cudaFree(d_res);
}

}

// BENCHMARK_CAPTURE takes the following arguments
// 1. benchmark function
// 2. benchmark name
BENCHMARK_CAPTURE(bench_axpy, d_axpy_single_thread, double{}, cuda::kernel::single_thread)->RangeMultiplier(4)->Range(2, 2 << 15)->Iterations(100);
BENCHMARK_CAPTURE(bench_axpy, d_axpy_parallel, double{}, cuda::kernel::parallel_one)->RangeMultiplier(4)->Range(2 << 14, 2 << 22)->Iterations(100);
BENCHMARK_CAPTURE(bench_norm2, d_norm2_single_thread, double{}, cuda::kernel::single_thread)->RangeMultiplier(4)->Range(2, 2 << 15)->Iterations(100);
BENCHMARK_CAPTURE(bench_norm2, d_norm2_atomic, double{}, cuda::kernel::parallel_one)->RangeMultiplier(4)->Range(2 << 14, 2 << 22)->Iterations(100);
BENCHMARK_CAPTURE(bench_norm2, d_norm2_reduction, double{}, cuda::kernel::parallel_two)->RangeMultiplier(4)->Range(2 << 14, 2 << 22)->Iterations(100);


BENCHMARK_MAIN();
