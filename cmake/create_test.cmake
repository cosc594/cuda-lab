function(sc4e_create_test test_name)
  file(RELATIVE_PATH REL_BINARY_DIR
       ${PROJECT_BINARY_DIR} ${CMAKE_CURRENT_BINARY_DIR})
  string(REPLACE "/" "_" TEST_TARGET_NAME "${REL_BINARY_DIR}/${test_name}")
  add_executable(${TEST_TARGET_NAME} ${test_name}.cpp)
  target_include_directories("${TEST_TARGET_NAME}"
                             PRIVATE
                             "$<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>"
                             "$<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>"
                             )
  set_target_properties(${TEST_TARGET_NAME} PROPERTIES
                        OUTPUT_NAME ${test_name})
  target_link_libraries(${TEST_TARGET_NAME} PUBLIC sc4e_options Ginkgo::ginkgo GTest::Main GTest::GTest ${ARGN})
  add_test(NAME ${REL_BINARY_DIR}/${test_name} COMMAND ${TEST_TARGET_NAME})
endfunction(sc4e_create_test)

function(sc4e_create_gbench bench_name)
  find_package(Threads REQUIRED)
  file(RELATIVE_PATH REL_BINARY_DIR
       ${PROJECT_BINARY_DIR} ${CMAKE_CURRENT_BINARY_DIR})
  string(REPLACE "/" "_" TEST_TARGET_NAME "${REL_BINARY_DIR}/${bench_name}")
  add_executable(${TEST_TARGET_NAME} ${bench_name}.cu)
  target_include_directories("${TEST_TARGET_NAME}"
                             PRIVATE
                             "$<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>"
                             "$<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>"
                             )
  set_target_properties(${TEST_TARGET_NAME} PROPERTIES
                        OUTPUT_NAME ${bench_name})
  target_link_libraries(${TEST_TARGET_NAME} PUBLIC sc4e_options Ginkgo::ginkgo GTest::Main GTest::GTest GBenchmark::GBenchmark Threads::Threads ${ARGN})
endfunction(sc4e_create_gbench)
