/*******************************<GINKGO LICENSE>******************************
Copyright (c) 2017-2023, the Ginkgo authors
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************<GINKGO LICENSE>*******************************/

#ifndef SC4E_INCLUDE_ENUM_HPP
#define SC4E_INCLUDE_ENUM_HPP

#include <algorithm>
#include <array>
#include <string_view>

namespace detail {

template<typename E>
constexpr std::array<E, static_cast<std::size_t>(E::COUNT)> build_all_kernels() {
  std::array<E, static_cast<std::size_t>(E::COUNT)> kernels{};
  for (std::size_t i = 0; i < kernels.size(); ++i) {
    kernels[i] = static_cast<E>(i);
  }
  return kernels;
}

template<std::size_t N>
constexpr std::array<std::string_view, N> split_enum(std::string_view s) {
  std::array<std::string_view, N> names{};
  auto it = s.data();
  std::size_t kernel_index = 0;
  for (auto it_end = s.begin(); it_end != s.end(); ++it_end) {
    if (*it_end == ',') {
      names[kernel_index] = {it, static_cast<std::size_t>(std::distance(it, it_end))};
      it = it_end + 1;
      kernel_index++;
    } else if (*it_end == ' ') {
      it = it_end + 1;
    }
  }
  names[kernel_index] = {it, static_cast<std::size_t>(std::distance(it, s.end()))};

  return names;
}

}// namespace detail

/*
 * This macro fills an enum called `kernel` with the specified values and
 * defines some helper variables.
 *
 * It defines `kernel_size` which holds the number of enum elements.
 * It defines `all_kernels` which is a container holding all enum elements.
 * It defines `all_kernel_names` which holds at position i the string name of the enum element `all_kernels[i]`.
 */
#define SC4E_KERNEL_VARIANTS(...)                                                                    \
  enum class kernel { __VA_ARGS__, COUNT };                                                             \
  constexpr std::size_t kernel_size = static_cast<std::size_t>(kernel::COUNT);                          \
  constexpr static auto all_kernels = detail::build_all_kernels<kernel>();                              \
  constexpr static auto all_kernel_names = detail::split_enum<kernel_size>(#__VA_ARGS__);               \
  inline std::string_view to_string(kernel k) { return all_kernel_names[static_cast<std::size_t>(k)]; } \
  static_assert(true, "Require ; after macro")

#endif//SC4E_INCLUDE_ENUM_HPP
