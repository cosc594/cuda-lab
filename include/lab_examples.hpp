#include "include/enum.hpp"
#include "include/types.hpp"

namespace cuda {

SC4E_KERNEL_VARIANTS(single_thread, parallel_one, parallel_two);

template<typename ValueType>
void axpy_single_thread(const ValueType* a, const ValueType* x, const size_type length,
                        ValueType* y);

template<typename ValueType>
void axpy_parallel(const ValueType* a, const ValueType* x, const size_type length,
                        ValueType* y);

template<typename ValueType>
void norm2_single_thread(const ValueType* vec, const size_type length,
                        ValueType* res);

template<typename ValueType>
void norm2_atomic(const ValueType* vec, const size_type length,
                        ValueType* res);

template<typename ValueType>
void norm2_reduction(const ValueType* vec, const size_type length,
                        ValueType* res);

template <typename ValueType>
void axpy(const ValueType* a, const ValueType* x, const size_type length,
          ValueType* y, kernel impl = kernel::single_thread);

template <typename ValueType>
void norm2(const ValueType* vec, const size_type length, ValueType* res,
           kernel impl = kernel::single_thread);

}// namespace cuda
