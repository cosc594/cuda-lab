#include <cuda_runtime.h>

#include "../include/lab_examples.hpp"
#include "include/cuda/exception.hpp"
#include "include/cuda/types.hpp"

namespace cuda {

constexpr unsigned int default_block_size = 512;

template <typename ValueType>
__global__ void axpy_kernel(const ValueType* __restrict__ a, const ValueType* __restrict__ x, const size_type length, ValueType* __restrict__ y)
{
    for (int i = 0; i < length; i++) {
        y[i] += a[0] * x[i];
    }
}

template<typename ValueType>
void axpy_single_thread(const ValueType* a, const ValueType* x, const size_type length,
                        ValueType* y) {
    //            grid_dim  block_dim   
    axpy_kernel<<<dim3(1),  dim3(1)>>>(a, x, length, y);
}

#define SC4E_DECLARE_CUDA_AXPY_SINGLE_THREAD(ValueType)                                     \
    void axpy_single_thread(const ValueType* a, const ValueType* x, const size_type length, \
          ValueType* y)

template <typename ValueType>
__global__ void axpy_kernel2(const ValueType* __restrict__ a, const ValueType* __restrict__ x, const size_type length, ValueType* __restrict__ y)
{
    auto tidx = threadIdx.x + blockDim.x * blockIdx.x;
    if (tidx < length) {
        y[tidx] += a[0] * x[tidx];
    } 
}

template<typename ValueType>
void axpy_parallel(const ValueType* a, const ValueType* x, const size_type length,
                        ValueType* y) {
    // Compute the correct grid dim such that you have enough threads to access one vector element per thread
    size_type grid_dim = (length + default_block_size - 1) / default_block_size;
    axpy_kernel2<<<dim3(grid_dim), dim3(default_block_size)>>>(a, x, length, y);
}

#define SC4E_DECLARE_CUDA_AXPY_PARALLEL(ValueType)                                     \
    void axpy_parallel(const ValueType* a, const ValueType* x, const size_type length, \
          ValueType* y)

template <typename ValueType>
__global__ void norm2_kernel(const ValueType* __restrict__ vec, const size_type length,
                             ValueType* __restrict__ res)
{
    *res = 0;
    for (int i = 0; i < length; i++) {
        *res += vec[i] * vec[i];
    }
    *res = sqrt(*res);
}

template<typename ValueType>
void norm2_single_thread(const ValueType* vec, const size_type length,
                        ValueType* res) {
    norm2_kernel<<<dim3(1), dim3(1)>>>(vec, length, res);
}

#define SC4E_DECLARE_CUDA_NORM2_SINGLE_THREAD(ValueType)                   \
    void norm2_single_thread(const ValueType* vec, const size_type length, \
          ValueType* y)

template <typename ValueType>
__global__ void norm2_kernel2(const ValueType* __restrict__ vec, const size_type length,
                             ValueType* __restrict__ res)
{
    const auto tidx = threadIdx.x + blockIdx.x * blockDim.x;
    if (tidx < length) {
        atomicAdd(res, vec[tidx] * vec[tidx]);
    } 
}

template <typename ValueType>
__global__ void finalize_norm(ValueType* __restrict__ res)
{
    res[0] = sqrt(res[0]);
}

template<typename ValueType>
void norm2_atomic(const ValueType* vec, const size_type length,
                        ValueType* res) {
    size_type grid_dim = (length + default_block_size - 1) / default_block_size;
    norm2_kernel2<<<dim3(grid_dim), dim3(default_block_size)>>>(vec, length, res);
    finalize_norm<<<dim3(1), dim3(1)>>>(res);
}

#define SC4E_DECLARE_CUDA_NORM2_ATOMIC(ValueType)                   \
    void norm2_atomic(const ValueType* vec, const size_type length, \
          ValueType* y)

template <typename ValueType>
__global__ void norm2_kernel3(const ValueType* __restrict__ vec, const size_type length,
                              ValueType* __restrict__ res)
{
    const auto tidx = threadIdx.x;
    const auto n_threads = blockDim.x;

    // Create shared memory array
    __shared__ ValueType tmp[default_block_size];
    tmp[tidx] = 0;

    for (int i = tidx; i < length; i += n_threads) {
        tmp[tidx] += vec[i] * vec[i];
    }
    __syncthreads();

    for (int i = default_block_size / 2; i > 0; i /= 2) {
        if (tidx < i) {
            tmp[tidx] += tmp[tidx + i];
        }
        __syncthreads();
    }

    if (tidx == 0) {
        *res = sqrt(tmp[0]);
    }
}

template<typename ValueType>
void norm2_reduction(const ValueType* vec, const size_type length,
                        ValueType* res) {
    norm2_kernel3<<<dim3(1), dim3(default_block_size)>>>(vec, length, res);
}

#define SC4E_DECLARE_CUDA_NORM2_REDUCTION(ValueType)                   \
    void norm2_reduction(const ValueType* vec, const size_type length, \
          ValueType* y)

template<typename ValueType>
void axpy(const ValueType* a, const ValueType* x, const size_type length,
          ValueType* y, kernel variant) {
    ValueType *d_a, *d_x, *d_y;
    cudaMalloc( (void **) &d_a, sizeof(ValueType) );
    cudaMalloc( (void **) &d_x, length * sizeof(ValueType) );
    cudaMalloc( (void **) &d_y, length * sizeof(ValueType) );
    cudaMemcpy(d_a, a, sizeof(ValueType), cudaMemcpyHostToDevice);
    cudaMemcpy(d_x, x, length * sizeof(ValueType), cudaMemcpyHostToDevice);
    cudaMemcpy(d_y, y, length * sizeof(ValueType), cudaMemcpyHostToDevice);
    if (variant == kernel::single_thread) {
        axpy_single_thread(d_a, d_x, length, d_y);
    } else if (variant == kernel::parallel_one || variant == kernel::parallel_two){
        axpy_parallel(d_a, d_x, length, d_y);
    } else{
        throw std::runtime_error("Not implemented");
    }
    cudaMemcpy(y, d_y, length * sizeof(ValueType), cudaMemcpyDeviceToHost);
    cudaFree(d_a);
    cudaFree(d_x);
    cudaFree(d_y);
    cudaDeviceSynchronize();
}

#define SC4E_DECLARE_CUDA_AXPY(ValueType)                  \
    void axpy(const ValueType* a, const ValueType* x, const size_type length, \
          ValueType* y, kernel variant)

template<typename ValueType>
void norm2(const ValueType* vec, const size_type length,
          ValueType* res, kernel variant) {
    *res = 0;
    ValueType *d_vec, *d_res;
    cudaMalloc( (void **) &d_res, sizeof(ValueType) );
    cudaMalloc( (void **) &d_vec, length * sizeof(ValueType) );
    cudaMemcpy(d_res, res, sizeof(ValueType), cudaMemcpyHostToDevice);
    cudaMemcpy(d_vec, vec, length * sizeof(ValueType), cudaMemcpyHostToDevice);
    if (variant == kernel::single_thread) {
        norm2_single_thread(d_vec, length, d_res);
    } else if (variant == kernel::parallel_one) {
        norm2_atomic(d_vec, length, d_res);
    } else if (variant == kernel::parallel_two){
        norm2_reduction(d_vec, length, d_res);
    } else {
        throw std::runtime_error("Not implemented");
    }
    cudaMemcpy(res, d_res, sizeof(ValueType), cudaMemcpyDeviceToHost);
    cudaFree(d_vec);
    cudaFree(d_res);
    cudaDeviceSynchronize();
}

#define SC4E_DECLARE_CUDA_NORM2(ValueType)                  \
    void norm2(const ValueType* vec, const size_type length, \
          ValueType* y, kernel variant)

SC4E_INSTANTIATE_FOR_EACH_NON_COMPLEX_VALUE_TYPE(SC4E_DECLARE_CUDA_AXPY_SINGLE_THREAD);
SC4E_INSTANTIATE_FOR_EACH_NON_COMPLEX_VALUE_TYPE(SC4E_DECLARE_CUDA_AXPY_PARALLEL);
SC4E_INSTANTIATE_FOR_EACH_NON_COMPLEX_VALUE_TYPE(SC4E_DECLARE_CUDA_AXPY);
SC4E_INSTANTIATE_FOR_EACH_NON_COMPLEX_VALUE_TYPE(SC4E_DECLARE_CUDA_NORM2_SINGLE_THREAD);
SC4E_INSTANTIATE_FOR_EACH_NON_COMPLEX_VALUE_TYPE(SC4E_DECLARE_CUDA_NORM2_ATOMIC);
SC4E_INSTANTIATE_FOR_EACH_NON_COMPLEX_VALUE_TYPE(SC4E_DECLARE_CUDA_NORM2_REDUCTION);
SC4E_INSTANTIATE_FOR_EACH_NON_COMPLEX_VALUE_TYPE(SC4E_DECLARE_CUDA_NORM2);

}// namespace cuda
