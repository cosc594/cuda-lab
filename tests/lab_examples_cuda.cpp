#include <gtest/gtest.h>
#include <ginkgo/ginkgo.hpp>

#include "test/utils.hpp"

#include "include/lab_examples.hpp"

template <typename T>
class Dense : public ::testing::Test {
protected:
    using value_type = T;
    using Vec = gko::matrix::Dense<value_type>;
    Dense()
        : exec(gko::ReferenceExecutor::create()),
          vec1(gko::initialize<Vec>({4.0, 2.0, 0.0, -1.0}, exec)),
          vec2(gko::initialize<Vec>({1.0, -2.0, 9.0, -2.0}, exec))
    {}

    std::shared_ptr<const gko::Executor> exec;
    std::unique_ptr<Vec> vec1;
    std::unique_ptr<Vec> vec2;

    void assert_equal_vectors(const Vec *vec_1, const Vec *vec_2)
    {
        ASSERT_EQ(vec_1->get_size(), vec_2->get_size());
        for (size_type i = 0; i < vec_1->get_size()[0]; ++i)
            EXPECT_EQ(vec_1->at(i), vec_2->at(i));
    }

    void test_axpy(cuda::kernel variant) {
        auto comp_res = Vec::create(this->exec, gko::dim<2>{1, 1});
        auto alpha = gko::initialize<Vec>({-2.0}, this->exec);
        auto vec = gko::clone(this->vec1);
        auto comp_vec = Vec::create(this->exec, this->vec1->get_size());

        this->vec1->add_scaled(alpha.get(), this->vec2.get());

        cuda::axpy(alpha->get_const_values(), this->vec2->get_const_values(),
                    this->vec1->get_size()[0], vec->get_values(), variant);

        this->assert_equal_vectors(vec.get(), this->vec1.get());
    }

    void test_norm2(cuda::kernel variant) {
        auto res = Vec::create(this->exec, gko::dim<2>{1, 1});
        auto comp_res = Vec::create(this->exec, gko::dim<2>{1, 1});
        this->vec1->compute_norm2(comp_res.get());

        norm2(this->vec1->get_const_values(),
            static_cast<size_type>(this->vec1->get_size()[0]), res->get_values(), variant);

        ASSERT_EQ(res->at(0), comp_res->at(0));
    }
};

TYPED_TEST_SUITE(Dense, gko::test::RealValueTypes);


TYPED_TEST(Dense, ComputesCorrectAxpySingleThread)
{
    this->test_axpy(cuda::kernel::single_thread);
}

TYPED_TEST(Dense, ComputesCorrectAxpyParallel)
{
    this->test_axpy(cuda::kernel::parallel_one);
}

TYPED_TEST(Dense, ComputesCorrectNorm2SingleThread)
{
    this->test_norm2(cuda::kernel::single_thread);
}

TYPED_TEST(Dense, ComputesCorrectNorm2Atomic)
{
    this->test_norm2(cuda::kernel::parallel_one);
}
TYPED_TEST(Dense, ComputesCorrectNorm2Reduction)
{
    this->test_norm2(cuda::kernel::parallel_two);
}
